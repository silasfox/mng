PREFIX = /usr/local

mng: mng.sh mng.awk mng.tsv
	cat mng.sh > $@
	echo 'exit 0' >> $@
	echo '#EOF' >> $@
	tar cz mng.awk mng.tsv >> $@
	chmod +x $@

test: mng.sh
	shellcheck -s sh mng.sh

clean:
	rm -f mng

install: mng
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f mng $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/mng

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/mng

.PHONY: test clean install uninstall
