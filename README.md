# mng

Read the Word of God from your terminal

Forked from [Luke Smith/kjv.git](https://gitlab.com/LukeSmithxyz/kjv) but with the Apocrypha added.


## Usage

    usage: ./mng [flags] [reference...]

      -l      list books
      -W      no line wrap
      -h      show help

      Reference types:
          <Book>
              Individual book
          <Book>:<Chapter>
              Individual chapter of a book
          <Book>:<Chapter>:<Verse>[,<Verse>]...
              Individual verse(s) of a specific chapter of a book
          <Book>:<Chapter>-<Chapter>
              Range of chapters in a book
          <Book>:<Chapter>:<Verse>-<Verse>
              Range of verses in a book chapter
          <Book>:<Chapter>:<Verse>-<Chapter>:<Verse>
              Range of chapters and verses in a book

          /<Search>
              All verses that match a pattern
          <Book>/<Search>
              All verses in a book that match a pattern
          <Book>:<Chapter>/<Search>
              All verses in a chapter of a book that match a pattern

## Build

mng can be built by cloning the repository and then running make:

    git clone https://gitlab.com/silasfox/mng.git
    cd mng
    sudo make install

## Further info

I generated the mng.tsv from [this xml version of the Menge translation](https://sourceforge.net/projects/zefania-sharp/files/Bibles/GER/Menge-Bibel/)
(Sourceforge). For that I used [this self written Raku tool](https://gitlab.com/silasfox/menge-creator.git).

## License

Public domain
